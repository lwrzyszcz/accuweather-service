package com.weather.accuweatherservice.api;


import com.weather.accuweatherservice.model.WeatherIn5DaysResponseModel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class AccuweatherControllerTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(
		AccuweatherControllerTests.class);

	TestRestTemplate restTemplate = new TestRestTemplate();
	HttpHeaders headers = new HttpHeaders();
	@Value("${local.server.port}")
	private int port;

	@Test
	public void get5daysWeatherTest() {
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ResponseEntity<WeatherIn5DaysResponseModel> response = restTemplate
			.exchange(createURLWithPort("/5daysWeather/22-554"), HttpMethod.GET, entity,
				WeatherIn5DaysResponseModel.class);
		final WeatherIn5DaysResponseModel body = response.getBody();
		Assert.assertNotNull(body);
		Assert.assertEquals(body.getStatus(), 200);
	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
}
