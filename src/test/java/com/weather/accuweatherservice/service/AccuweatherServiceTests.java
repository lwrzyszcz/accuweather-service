package com.weather.accuweatherservice.service;

import com.weather.accuweatherservice.model.LocationModel;
import com.weather.accuweatherservice.model.WeatherIn5DaysResponseModel;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@SpringBootTest
@ActiveProfiles("test")
public class AccuweatherServiceTests extends AbstractJUnit4SpringContextTests {

	@Autowired
	private AccuweatherService accuweatherService;

	@Test
	public void get5DaysWeatherByPostalCodeTest() {
		String testPostalCode = "22-554";
		WeatherIn5DaysResponseModel daysWeatherByPostalCode = null;
		try {
			daysWeatherByPostalCode = accuweatherService
				.get5DaysWeatherByPostalCode(testPostalCode);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull(daysWeatherByPostalCode);
		Assert.assertEquals(daysWeatherByPostalCode.getStatus(), 200);
	}

	@Test
	public void findLocationCodeByPostalCodeTest() {

		String testPostalCode = "22-554";
		LocationModel locationCodeByPostalCode = null;
		try {
			locationCodeByPostalCode = accuweatherService
				.findLocationCodeByPostalCode(testPostalCode);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
		Assert.assertNotNull(locationCodeByPostalCode);
	}
}
