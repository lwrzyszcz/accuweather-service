package com.weather.accuweatherservice.utils;

import com.weather.accuweatherservice.model.TemperatureValueModel;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@SpringBootTest
@ActiveProfiles("test")
public class TemperatureUtilsTests {

	@Test
	public void kelvinToCelsiusTest() {
		TemperatureValueModel temp1 = new TemperatureValueModel();
		temp1.setUnit("K");
		temp1.setValue(273);
		final int t1 = TemperatureUtils.convertToCelsius(temp1);
		Assert.assertEquals(t1, 0);

		TemperatureValueModel temp2 = new TemperatureValueModel();
		temp2.setUnit("K");
		temp2.setValue(310);
		final int t2 = TemperatureUtils.convertToCelsius(temp2);
		Assert.assertEquals(t2, 37);
	}

	@Test
	public void fahrenheitToCelsiusTest() {
		TemperatureValueModel temp1 = new TemperatureValueModel();
		temp1.setUnit("F");
		temp1.setValue(32);
		final int t1 = TemperatureUtils.convertToCelsius(temp1);
		Assert.assertEquals(t1, 0);

		TemperatureValueModel temp2 = new TemperatureValueModel();
		temp2.setUnit("F");
		temp2.setValue(50);
		final int t2 = TemperatureUtils.convertToCelsius(temp2);
		Assert.assertEquals(t2, 10);
	}
}
