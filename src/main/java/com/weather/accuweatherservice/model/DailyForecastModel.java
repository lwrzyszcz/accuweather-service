package com.weather.accuweatherservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Getter
@Setter
public class DailyForecastModel {

	@JsonProperty("Date")
	private Date date;
	@JsonProperty("EpochDate")
	private Long epochDate;
	@JsonProperty("Temperature")
	private TemperatureModel temperature;
	@JsonProperty("Day")
	private WeatherDescriptionModel day;
	@JsonProperty("Night")
	private WeatherDescriptionModel night;
}
