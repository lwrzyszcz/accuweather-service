package com.weather.accuweatherservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Getter
@Setter
public class WeatherResponseModel {
	@JsonProperty("DailyForecasts")
	private List<DailyForecastModel> dailyForecasts;
}
