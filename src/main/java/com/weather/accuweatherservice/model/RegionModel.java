package com.weather.accuweatherservice.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class RegionModel {
	@JsonProperty("ID")
	private String id;
	@JsonProperty("LocalizedName")
	private String localizedName;
	@JsonProperty("EnglishName")
	private String englishName;
	@JsonProperty("LocalizedType")
	private String localizedType;
	@JsonProperty("EnglishType")
	private String englishType;
}
