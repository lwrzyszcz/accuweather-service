package com.weather.accuweatherservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Getter
@Setter
public class LocationModel {

	@JsonProperty("Version")
	private Integer version;
	@JsonProperty("Key")
	private String key;
	@JsonProperty("Type")
	private String type;
	@JsonProperty("Rank")
	private Integer rank;
	@JsonProperty("LocalizedName")
	private String localizedName;
	@JsonProperty("EnglishName")
	private String englishName;
	@JsonProperty("PrimaryPostalCode")
	private String primaryPostalCode;
	@JsonProperty("Region")
	private RegionModel region;
	@JsonProperty("Country")
	private RegionModel country;
	@JsonProperty("AdministrativeArea")
	private RegionModel administrativeArea;
}
