package com.weather.accuweatherservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Getter
@Setter
public class WeatherDescriptionModel {
	@JsonProperty("Icon")
	private int icon;
	@JsonProperty("IconPhrase")
	private String iconPhrase;
}
