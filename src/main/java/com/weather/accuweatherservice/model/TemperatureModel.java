package com.weather.accuweatherservice.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Getter
@Setter
public class TemperatureModel {

	@JsonProperty("Minimum")
	private TemperatureValueModel minimum;
	@JsonProperty("Maximum")
	private TemperatureValueModel maximum;
}
