package com.weather.accuweatherservice.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Getter
@Setter
public class WeatherIn5DaysResponseModel {

	private String city;
	private String postalCode;
	private String administrativeArea;
	private String country;
	private List<DayWeatherResponseModel> days;
	private int status;
	private String errorMessage;
}
