package com.weather.accuweatherservice.model;

import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Getter
@Setter
public class DayWeatherResponseModel {

	private Date date;
	private String day;
	private int maxTemp;
	private int minTemp;
	private String weatherAtDayDescription;
	private String weatherAtNightDescription;
}
