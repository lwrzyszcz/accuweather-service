package com.weather.accuweatherservice.utils;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
public class FahrenheitToCelsiusConverter {

	public static double convert(int fahrenheit) {
		double celsius = (fahrenheit-32)*(0.5556);
		return celsius;
	}
}
