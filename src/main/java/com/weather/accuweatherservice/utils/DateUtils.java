package com.weather.accuweatherservice.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
public class DateUtils {

	public static String dayName(Date date) {
		return new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date);
	}
}
