package com.weather.accuweatherservice.utils;

import com.weather.accuweatherservice.model.TemperatureValueModel;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
public class TemperatureUtils {
	public static int convertToCelsius(TemperatureValueModel temp) {
		if("F".equals(temp.getUnit())) {
			return (int) Math.round(FahrenheitToCelsiusConverter.convert(temp.getValue()));
		} else if ("K".equals(temp.getUnit())) {
			return KelvinToCelsiusConverter.convert(temp.getValue());
		} else {
			return temp.getValue();
		}
	}
}
