package com.weather.accuweatherservice.utils;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
public class KelvinToCelsiusConverter {

	public static int convert(int kelvin) {
		int celsius = kelvin - 273;
		return celsius;
	}
}
