package com.weather.accuweatherservice.repository;

import com.weather.accuweatherservice.entity.AccuweatherRequests;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@org.springframework.stereotype.Repository
public interface AccuweatherRequestsDao extends CrudRepository<AccuweatherRequests, Long> {

	@Query(value = "select count(*) from accuweather_requests a",
		nativeQuery = true)
	int getCountQuery();
}
