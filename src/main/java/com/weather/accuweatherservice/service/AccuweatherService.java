package com.weather.accuweatherservice.service;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.accuweatherservice.entity.AccuweatherRequests;
import com.weather.accuweatherservice.model.DailyForecastModel;
import com.weather.accuweatherservice.model.DayWeatherResponseModel;
import com.weather.accuweatherservice.model.LocationModel;
import com.weather.accuweatherservice.model.WeatherIn5DaysResponseModel;
import com.weather.accuweatherservice.model.WeatherResponseModel;
import com.weather.accuweatherservice.repository.AccuweatherRequestsDao;
import com.weather.accuweatherservice.utils.DateUtils;
import com.weather.accuweatherservice.utils.TemperatureUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Service
public class AccuweatherService {

	private static final Logger LOGGER = LoggerFactory.getLogger(
		AccuweatherService.class);

	@Autowired
	private AccuweatherRequestsDao requestsDao;

	@Value("${accuweather.api.key}")
	private String apiKey;
	@Value("${accuweather.api.language}")
	private String language;
	@Value("${accuweather.api.host}")
	private String host;
	@Value("${accuweather.api.5daysWeatherEndpoint}")
	private String weatherIn5DaysEndpoint;
	@Value("${accuweather.api.findLocationEndpoint}")
	private String findLocationEndpoint;

	private ObjectMapper objectMapper = new ObjectMapper()
		.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(
			MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true)
		.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);

	public WeatherIn5DaysResponseModel get5DaysWeatherByPostalCode(String postalCode)
		throws Exception {
		final LocationModel location = this.findLocationCodeByPostalCode(postalCode);
		String locationCode = location.getKey();
		WeatherIn5DaysResponseModel weatherIn5Days = new WeatherIn5DaysResponseModel();
		WeatherResponseModel response = null;
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<WeatherResponseModel> serverResponse =
				restTemplate.exchange(
					host + weatherIn5DaysEndpoint + locationCode + "?apikey=" + apiKey
						+ "&language="
						+ language, HttpMethod.GET, null,
					WeatherResponseModel.class);
			response = serverResponse.getBody();
			AccuweatherRequests requests = new AccuweatherRequests();
			requests.setDate(new Date());
			requestsDao.save(requests);
			LOGGER.info("Got response from accuweather: " + serverResponse);
		} catch (Exception e) {
			LOGGER.error("Get weather failed for location code: " + locationCode);
			throw e;
		}
		mapToResponseModel(weatherIn5Days, response, location);
		return weatherIn5Days;
	}

	public LocationModel findLocationCodeByPostalCode(String postalCode) throws Exception {
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<LocationModel[]> serverResponse =
				restTemplate.exchange(
					host + findLocationEndpoint + "?apikey=" + apiKey + "&q=" + postalCode + "&language="
						+ language, HttpMethod.GET, null,
					LocationModel[].class);

			LocationModel[] locations = serverResponse.getBody();
			AccuweatherRequests requests = new AccuweatherRequests();
			requests.setDate(new Date());
			requestsDao.save(requests);
			LOGGER.info("Got location response from accuweather: " + serverResponse);
			if (locations == null || locations.length == 0) {
				throw new Exception("Location not fond for postal code: " + postalCode + " Please check if value is correct.");
			}
			return locations[0];
		} catch (Exception e) {
			LOGGER.error("Get location by postal code failed for postal code: " + postalCode);
			throw new Exception(e.getMessage());
		}
	}

	public int getRequestsCount() {
		return requestsDao.getCountQuery();
	}

	private void mapToResponseModel(WeatherIn5DaysResponseModel weatherIn5Days,
		WeatherResponseModel response, LocationModel location) throws Exception {
		if (response != null && response.getDailyForecasts() != null) {
			List<DayWeatherResponseModel> days = new ArrayList<>();
			for (DailyForecastModel dailyForecastModel : response.getDailyForecasts()) {
				DayWeatherResponseModel day = new DayWeatherResponseModel();
				day.setDate(dailyForecastModel.getDate());
				day.setDay(DateUtils.dayName(dailyForecastModel.getDate()));
				day.setMaxTemp(
					TemperatureUtils.convertToCelsius(dailyForecastModel.getTemperature().getMaximum()));
				day.setMinTemp(
					TemperatureUtils.convertToCelsius(dailyForecastModel.getTemperature().getMinimum()));
				day.setWeatherAtDayDescription(dailyForecastModel.getDay().getIconPhrase());
				day.setWeatherAtNightDescription(dailyForecastModel.getNight().getIconPhrase());
				days.add(day);
			}
			weatherIn5Days.setCity(location.getLocalizedName());
			weatherIn5Days.setPostalCode(location.getPrimaryPostalCode());
			weatherIn5Days.setAdministrativeArea(location.getAdministrativeArea().getLocalizedName());
			weatherIn5Days.setCountry(location.getCountry().getLocalizedName());
			weatherIn5Days.setDays(days);
			weatherIn5Days.setStatus(HttpStatus.OK.value());
		} else {
			throw new Exception("No daily rocast was fond for location: " + location.getKey());
		}
	}

}
