package com.weather.accuweatherservice.api;

import com.weather.accuweatherservice.model.WeatherIn5DaysResponseModel;
import com.weather.accuweatherservice.service.AccuweatherService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Łukasz Wrzyszcz <lukasz.wrzyszcz@teaminternational.com>
 */
@Api
@Component
@RestController("accuweather")
@Scope("request")
public class AccuweatherController {

	@Autowired
	private AccuweatherService accuweatherService;

	@GetMapping("5daysWeather/{postalCode}")
	public ResponseEntity<WeatherIn5DaysResponseModel> get5daysWeather(
		@PathVariable String postalCode) {
		try {
			final WeatherIn5DaysResponseModel weatcherIn5Days = this.accuweatherService
				.get5DaysWeatherByPostalCode(postalCode);
			return new ResponseEntity<WeatherIn5DaysResponseModel>(weatcherIn5Days,
				HttpStatus.valueOf(weatcherIn5Days.getStatus()));
		} catch (Exception e) {
			e.printStackTrace();
			WeatherIn5DaysResponseModel weatcherIn5Days = new WeatherIn5DaysResponseModel();
			weatcherIn5Days.setErrorMessage(e.getMessage());
			weatcherIn5Days.setStatus(HttpStatus.BAD_REQUEST.value());
			return new ResponseEntity<WeatherIn5DaysResponseModel>(weatcherIn5Days,
				HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("requestsCount")
	public ResponseEntity<Integer> getRequestsCount() {
		return new ResponseEntity<Integer>(accuweatherService.getRequestsCount(), HttpStatus.OK);
	}
}
