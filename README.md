# Read Me 

##Instrukcja instalacji:

W celu instalacji mikroserwisu na produkcji należy w pierwszej kolejności należy ustawić poprawne
wartości w pliku application-prod.properties. Można to zrobic ustawiając zmienne w środowisku w którym będzie uruchomina. 
Aplikacja na produkcji powinna byc uruchomiona z profilem prod.

W pliku application.properties są ustawione przykładowe wartości zmiennych. Na tych ustawieniach użzywałem aplikacji.
Aby aplikacja użyła ustawien z pliku application properties nalezy ją uruchomiś bez profilu (default)

Uruchamiając aplikację z profilem test korzystamy z endpointów zamockowanych w serwisie https://run.mocky.io
a nie z serwisu accuweather. Poniewaz w accuweather jest limit zapytań, łatwiej było testować na poczatku z mocków.

Lokalnie możemy wystartowac AccuweatherServiceApplication. Dodałem swaggera przez którego można przetestować jej działanie.
Adres swaggera: http://localhost:8080/swagger-ui.html
